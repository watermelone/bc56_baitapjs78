var arrayNumber = []
document.getElementById("btn-add-number").onclick = function () {
    var soN = document.getElementById("soN").value;
    arrayNumber.push(soN)
    document.getElementById("result_array").innerHTML = arrayNumber
}

// yeu cau 1
document.getElementById("btnBai1").onclick = function () {
    var result1 = 0;
    for (var i = 0; i < arrayNumber.length; i++) {
        result1 = result1 + arrayNumber[i] * 1;
    }
    document.getElementById("result1").innerHTML = result1
}

// yeu cau 2
document.getElementById("btnBai2").onclick = function () {
    var count = 0;
    for (var i = 0; i < arrayNumber.length; i++) {
        if (arrayNumber[i] % 2 === 0 && arrayNumber[i] !== 0) {
            count++
        }
    }
    document.getElementById("result2").innerHTML = "Số dương trong mảng là: " + count
}

// yeu cau 3
document.getElementById("btnBai3").onclick = function () {
    var min = arrayNumber[0] * 1;
    for (var i = 0; i < arrayNumber.length; i++) {
        if (arrayNumber[i] < min) {
            min = arrayNumber[i] * 1
        }
    }
    document.getElementById("result3").innerHTML = "Số nhỏ nhất trong mảng là : " + min
}

// yeu cau 4
document.getElementById("btnBai4").onclick = function () {
    var min = 0;
    var arraySoDuong = []
    for (var i = 0; i < arrayNumber.length; i++) {
        if (arrayNumber[i] % 2 === 0 && arrayNumber[i] !== 0) {
            arraySoDuong.push(arrayNumber[i])
        }
    }

    min = arraySoDuong[0] * 1;
    for (var k = 0; k < arraySoDuong.length; k++) {
        if (arraySoDuong[k] < min) {
            min = arraySoDuong[k] * 1
            console.log(min);
        }
    }
    document.getElementById("result4").innerHTML = "Số dương nhỏ nhất trong mảng là : " + min
}

// yeu cau 5
document.getElementById("btnBai5").onclick = function () {
    var arraySoDuong = []
    for (var i = 0; i < arrayNumber.length; i++) {
        if (arrayNumber[i] % 2 === 0 && arrayNumber[i] !== 0) {
            arraySoDuong.push(arrayNumber[i])
        }
    }

    if (arraySoDuong.length !== 0) {
        document.getElementById("result5").innerHTML = "Số dương cuối cùng trong mảng là : " + arraySoDuong[arraySoDuong.length - 1]
    } else {
        document.getElementById("result5").innerHTML = "-1"
    }
}

// yeu cau 6
document.getElementById("btn-change-index").onclick = function () {
    var viTri1 = document.getElementById("viTri1").value * 1;
    var viTri2 = document.getElementById("viTri2").value * 1;
    var temp1 = 0;
    var temp2 = 0;
    for (var i = 1; i <= arrayNumber.length; i++) {
        if (i === viTri1) {
            console.log(i - 1);
            temp1 = arrayNumber[i - 1] * 1;


        }
        if (i === viTri2) {
            console.log(i - 1);
            temp2 = arrayNumber[i - 1] * 1;


        }
    }
    arrayNumber[viTri1 - 1] = temp2;
    arrayNumber[viTri2 - 1] = temp1;
    document.getElementById("result6").innerHTML = "Mảng sau khi đã đổi 2 vị trí " + viTri1 + " và " + viTri2 + " là: " + arrayNumber
}

// yeu cau 7
document.getElementById("btnBai7").onclick = function () {
    for (var i = 0; i < arrayNumber.length - 1; i++) {
        for (var j = i + 1; j < arrayNumber.length; j++) {
            if (arrayNumber[i] * 1 > arrayNumber[j] * 1) {
                // Hoan vi 2 so a[i] va a[j]
                tg = arrayNumber[i];
                arrayNumber[i] = arrayNumber[j];
                arrayNumber[j] = tg;
            }
        }
    }
    document.getElementById("result7").innerHTML = "Mảng sau khi đã sắp xếp là: " + arrayNumber
}

// yeu cau 8
function kiemTraSoNguyenTo(n) {


    let flag = 1;

    if (n < 2) return flag = 0;
    let i = 2;
    while (i < n) {
        if (n % i == 0) {
            flag = 0;
            break;
        }
        i++;
    }

    return flag;
}

document.getElementById("btnBai8").onclick = function () {
    var ketqua = 0;
    var check = 0;
    for (var i = 0; i < arrayNumber.length; i++) {

        if (kiemTraSoNguyenTo(arrayNumber[i]) == 1) {
            check++;
            console.log(arrayNumber[i]);
            ketqua = arrayNumber[i] * 1;
            break;
        }

    }
    if (check != 0) {

        document.getElementById("result8").innerHTML = "Số nguyên tố đầu tiên trong mảng là: " + ketqua
    } else {
        document.getElementById("result8").innerHTML = "-1"
    }
}


// yeu cau bai 9

document.getElementById("btnThemSoBai9").onclick = function () {
    var soBai9 = document.getElementById("soBai9").value;
    arrayNumber.push(soBai9)
    document.getElementById("resultArray9").innerHTML = arrayNumber
}

document.getElementById("btnDemSoBai9").onclick = function(){
    var mangSoNguyen=[];
    for(var i=0;i<arrayNumber.length;i++){
        if(Number.isInteger(arrayNumber[i]*1)==true){
            mangSoNguyen.push(arrayNumber[i]*1)
        }
    }
    document.getElementById("resultbai9").innerHTML = "Số nguyên trong mảng là: "+mangSoNguyen.length
}

// yeu cau 10
document.getElementById("btnBai10").onclick = function(){
    var arraySoDuong=[];
    var arraySoAm=[];
    for(var i=0;i<arrayNumber.length;i++){
       if(arrayNumber[i]*1>0&&arrayNumber[i]*1!==0){
        arraySoDuong.push(arrayNumber[i])
       }else if(arrayNumber[i]*1<0&&arrayNumber[i]*1!==0){
        arraySoAm.push(arrayNumber[i])
       }
    }
    if(arraySoDuong.length>arraySoAm.length){
        document.getElementById("result10").innerHTML = "Số dương > Số âm";
    }else if(arraySoDuong.length<arraySoAm.length){
        document.getElementById("result10").innerHTML = "Số âm > Số dương";
    }else{
        document.getElementById("result10").innerHTML = "Số âm = Số dương";
    }
}